#!/usr/bin/env python3

import os
import sys
import argparse
import sqlite3
import gzip

class ANN_VCF:
    """Read and parse snpEff annotated VCFs"""

    def __init__(self, genes, filepath):
        self.path = filepath
        self.accession = os.path.basename(filepath).split(".")[0]
        self.orfs = []
        if genes is not None:
            self.orfs = genes.split(",")
        self.aa_variants = []

    def parse(self):
        try:
            if self.path[-3:] == '.gz':
                inputvcf = gzip.open(self.path, mode='rt')
            else:
                inputvcf = open(self.path, "r")
        except Exception as e:
            sys.exit(f"Error: {self.path} not found")
        else:
            aa_encode = {"Ala": "A", "Asx": "B", "Cys": "C",
                         "Asp": "D", "Glu": "E", "Phe": "F",
                         "Gly": "G", "His": "H", "Ile": "I",
                         "Lys": "K", "Leu": "L", "Met": "M",
                         "Asn": "N", "Pro": "P", "Gln": "Q",
                         "Arg": "R", "Ser": "S", "Thr": "T",
                         "Val": "V", "Trp": "W", "Xaa": "X",
                         "Tyr": "Y", "Glx": "Z"}
            for line in inputvcf:
                if not line.startswith("#"):
                    cols = line.strip().split("\t")
                    # require passing the filter
                    if cols[6] == "PASS":
                        info_items = cols[7].split(";")
                        for i in range(len(info_items)):
                            if info_items[i].startswith("ANN="):
                                annotations = info_items[i][4:].split(",")
                                protein_ann_orig = []
                                # ANN=T|missense_variant|MODERATE|S|GU280_gp02|transcript|GU280_gp02|protein_coding|1/1|c.1501A>T|p.Asn501Tyr|1501/3822|1501/3822|501/1273|  |
                                #     1|       2        |    3   |4|    5     |    6     |    7     |      8       | 9 |    10   |    11     |   12    |   13    |   14   |15| 16
                                for ann in annotations:
                                    fields = ann.split("|")
                                    if not self.orfs or fields[3] in self.orfs:
                                        if fields[10] not in protein_ann_orig:
                                            protein_ann_orig.append(fields[10])
                                            mut = fields[10][2:]
                                            for three in aa_encode.keys():
                                                mut = mut.replace(three, aa_encode[three])
                                            self.aa_variants.append(f"{fields[3]}:{mut}")
            inputvcf.close()
        return

    def prepare_insert(self):
        if self.aa_variants:
            self.ins_values = (self.accession, ",".join(self.orfs), ",".join(self.aa_variants))
        else:
            self.ins_values = (self.accession, ",".join(self.orfs), None)

def init_sqlite_table(db):
    conn = sqlite3.connect(db)
    cur = conn.cursor()

    cur.execute('''CREATE TABLE IF NOT EXISTS variants
        (accession TEXT PRIMARY KEY,
        orfs TEXT,
        aa_variants TEXT);''')
    conn.commit()
    conn.close()
    return

parser = argparse.ArgumentParser(
    description='Parse and insert snpEff protein variants')
parser.add_argument(
    '-i',
    dest="filepath",
    help='Path to annotated VCF file')
parser.add_argument(
    '-g',
    dest="products",
    help='Comma separated list of gene/cds/protein')
parser.add_argument(
    '-d',
    dest="database",
    help="Sqlite database"
)
parser.add_argument(
    '-setup',
    dest="setup",
    action='store_true',
    help="Initialize sqlite database"
)
args = parser.parse_args()

if args.filepath is None:
    if args.setup:
        init_sqlite_table(args.database)
        print("Database initialized")
        sys.exit()
    else:
        sys.exit(f"Error: VCF filepath needed")
else:
    sample = ANN_VCF(args.products, args.filepath)
    sample.parse()
    sample.prepare_insert()
    conn = sqlite3.connect(args.database)
    cur = conn.cursor()

    cur.execute('''CREATE TABLE IF NOT EXISTS variants
        (accession TEXT PRIMARY KEY,
        orfs TEXT,
        aa_variants TEXT);''')
    conn.commit()
    cur.execute('''INSERT OR REPLACE INTO variants (accession, orfs, aa_variants) VALUES (?,?,?)''', sample.ins_values)
    conn.commit()
    conn.close()

#print(sample.accession, sample.aa_variants)
