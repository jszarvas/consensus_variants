#!/bin/bash

#
# Script for selecting accession that don't have pango-lineage
#

DATABASE=$1
FILENAME=$2

sqlite3 $DATABASE 'select accession from samples where included is Null and accession not in (select accession from pango_lineage);' > $FILENAME
sqlite3 $DATABASE 'select accession from samples where included=1 and accession not in (select accession from pango_lineage);' >> $FILENAME
