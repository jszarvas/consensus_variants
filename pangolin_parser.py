#!/usr/bin/env python3

import os
import sys
import argparse
import json
import sqlite3
import requests
from time import sleep

class BATCH_PANGOLIN:
    """Read and parse pangolin outputted csv"""

    def __init__(self, path):
        self.path = path
        self.ins_values = []


    def parse_csv(self):
        try:
            inputcsv = open(self.path, "r")
        except Exception as e:
            sys.exit(f"Error: {self.path} not found")
        else:
            # throw header away
            _ = inputcsv.readline()
            for line in inputcsv:
                # taxon|lineage|conflict|ambiguity_score|scorpio_call|scorpio_support|scorpio_conflict|version|pangolin_version|pangoLEARN_version|pango_version|status|note
                #  0   |   1   |    2   |      3        |      4     |       5       |       6        |   7   |        8       |         9        |     10      |  11  | 12

                cols = line.strip().split(",")
                # require passing the qc
                if cols[11] == "passed_qc":
                    if cols[0].startswith("ENA|"):
                        sampleid = cols[0].split("|")[1]
                    else:
                        sampleid = cols[0]
                    self.ins_values.append((sampleid, cols[1]))
            inputcsv.close()
        return

    def parse_json(self):
        response = requests.get(self.path)
        if response.status_code == 200:
            pango_obj = response.json().get("results")
            for unit in pango_obj:
                if unit.get("has_lineage"):
                    self.ins_values.append((unit["id"], unit["lineage"]))
        return

class EBISEARCH_PANGOLIN:
    """Query EBI search for pangolin results"""

    def __init__(self, searchurl):
        self.searchurl = searchurl
        self.ins_values = []
        self.query_url = ""

    def parse_inputlist(self, filename):
        query_limit = 10
        with open(filename, "r") as infile:
            query_size = 0
            query_elements = []
            for line in infile:
                item = line.strip()
                if item:
                    query_elements.append(item)
                    query_size += 1
                    if query_size > query_limit:
                        self.query_url = "{}{}".format(self.searchurl, "%20OR%20".join(query_elements))
                        self.query_ebisearch()
                        sleep(1)
                        query_size = 0
                        query_elements = []
            if query_elements:
                self.query_url = "{}{}".format(self.searchurl, "%20OR%20".join(query_elements))
                self.query_ebisearch()
        return

    def query_ebisearch(self):
        response = requests.get(self.query_url)
        if response.status_code == 200:
            # {"hitCount":2,"entries":[{"acc":"LC633762","id":"LC633762","source":"lineage-covid19","fields":{"lineage":["B.1.1.7"]}},{"acc":"MW993017","id":"MW993017","source":"lineage-covid19","fields":{"lineage":["B.1.422"]}}],"facets":[]}
            pango_obj = response.json().get("entries")
            for unit in pango_obj:
                if unit.get("fields") and unit["fields"].get("lineage"):
                    self.ins_values.append((unit["id"], unit["fields"]["lineage"][0]))
        return


def init_sqlite_table(db):
    conn = sqlite3.connect(db)
    cur = conn.cursor()

    cur.execute('''CREATE TABLE IF NOT EXISTS pango_lineage
        (accession TEXT PRIMARY KEY,
        lineage TEXT);''')
    conn.commit()
    conn.commit()
    conn.close()
    return

parser = argparse.ArgumentParser(
    description='Helper script for getting pangolin results into database')
parser.add_argument(
    '-i',
    dest="filepath",
    help='Path to pangolin csv file')
parser.add_argument(
    '-l',
    dest="listpath",
    help='Path to list of accessions')
parser.add_argument(
    '-j',
    dest="jsonurl",
    help='API url serving json')
parser.add_argument(
    '-d',
    dest="database",
    help="Sqlite database"
)
parser.add_argument(
    '-setup',
    dest="setup",
    action='store_true',
    help="Initialize sqlite database"
)
args = parser.parse_args()

if args.filepath is None and args.jsonurl is None:
    if args.setup:
        init_sqlite_table(args.database)
        print("Database initialized")
        sys.exit()
    else:
        sys.exit(f"Error: csv/json path needed")
else:
    if args.filepath is not None:
        pango_lineage = BATCH_PANGOLIN(args.filepath)
        pango_lineage.parse_csv()
    elif args.jsonurl is not None:
        if args.listpath is not None:
            # https://www.ebi.ac.uk/ebisearch/ws/rest/lineage-covid19/?fields=lineage&format=JSON&query=
            pango_lineage = EBISEARCH_PANGOLIN(args.jsonurl)
            pango_lineage.parse_inputlist(args.listpath)
        else:
            pango_lineage = BATCH_PANGOLIN(args.jsonurl)
            pango_lineage.parse_json()
    conn = sqlite3.connect(args.database)
    cur = conn.cursor()

    cur.execute('''CREATE TABLE IF NOT EXISTS pango_lineage
        (accession TEXT PRIMARY KEY,
        lineage TEXT);''')
    conn.commit()
    cur.executemany('''INSERT OR REPLACE INTO pango_lineage (accession, lineage) VALUES (?,?)''', pango_lineage.ins_values)
    # # delete those that are not in a tree
    # cur.execute('''DELETE from pango_lineage where accession not in (select accession from samples where included = 1);''')
    conn.commit()
    conn.close()

print(f"# Inserted {len(pango_lineage.ins_values)} lineage labels")
