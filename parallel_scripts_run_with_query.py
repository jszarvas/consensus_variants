#!/usr/bin/env python3

import sys
import os
import shlex
import subprocess
import multiprocessing
import argparse
import sqlite3

def call_cmd(arguments):
    cmd = f"{arguments[0]} {arguments[1]}"
    p = subprocess.run(cmd, shell=True)
    return p.returncode

def query_database(db, query):
    conn = sqlite3.connect(db)
    cur = conn.cursor()

    units = []
    cur.execute(query)
    rows = cur.fetchall()
    if rows is not None:
        for r in rows:
            units.append(r[0])
    conn.close()
    return units

def couple_units(script, units):
    scr = [script for i in range(len(units))]
    return list(zip(scr, units))

parser = argparse.ArgumentParser(
    description='Parallelize some script based on sqlite query')
parser.add_argument(
    '-database',
    dest="database",
    required=True,
    help="Sqlite database"
)
parser.add_argument(
    '-query',
    dest="query",
    required=True,
    help="Query to database"
)
parser.add_argument(
    '-script',
    dest="script",
    required=True,
    help="Script to run"
)
parser.add_argument(
    '-p',
    dest="cpu",
    type=int,
    default=8,
    help='Number of parallel processes')
args = parser.parse_args()

MAX_JOBS = 20
n_jobs = min(MAX_JOBS, args.cpu)

job_units = query_database(args.database, args.query)

full_args = couple_units(args.script, job_units)

if full_args:
    if __name__ == '__main__':

        p = multiprocessing.Pool(n_jobs)
        p.imap_unordered(call_cmd, full_args)
        p.close()
        p.join()

sys.exit()
